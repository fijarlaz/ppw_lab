from django.db import models
from datetime import datetime, date
# Create your models here.

class Schedule(models.Model):
    hari_kegiatan = models.CharField(max_length=20)
    waktu_kegiatan = models.DateField()
    jam_kegiatan = models.TimeField()
    nama_kegiatan = models.CharField(max_length = 50)
    tempat_kegiatan = models.CharField(max_length = 50)
    kategori = models.CharField(max_length=100)
    def publish(self):
        self.save()
    
    def __str__(self):
        return self.nama_kegiatan