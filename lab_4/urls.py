from . import views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('contact/', views.contact, name='contact'),
    path('about/', views.about, name='about'),
    path('experiences/', views.experiences, name='experiences'),
    path('', views.index, name='index'),
    path('make-schedule', views.schedule, name='schedule'),
    path('my-schedule', views.schedule_save, name='schedule_save'),
    path('delete-schedule', views.delete_schedule, name='delete_schedule'),
    path('exp/', views.exp, name='exp'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
