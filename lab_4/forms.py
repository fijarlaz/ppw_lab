from django import forms
from datetime import datetime, time
from .models import Schedule

class ScheduleForm(forms.Form):
    error_message = {
        'required' : 'Please fill out this field',
        'invalid'  : 'Please fill with the valid input'

    }
    date_attrs = {
        'type' : 'date',
        'class' : 'todo-form-input',
    }
    place_attrs = {
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Tempat Kegiatan ..',
    }

    nama_kegiatan_attrs = {
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Aktivitas ..',
    }
    tempat_kegiatan_attrs = {
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Tempat kegiatan ..'
    }
    hari_kegiatan={
        'type' : 'text',
        'class': 'todo-form-input',
        'Placeholder':'Hari kegiatan ..'
    }
    OPTION= (
        ('Olahraga', 'Olahraga'),
        ('Kesenian', 'Kesenian'),
        ('Pendidikan', 'Pendidikan'),
        ('Lainnya', 'Lainnya')
    )
    hari_kegiatan = forms.CharField(max_length=20,required=True, widget=forms.TextInput() )
    waktu_kegiatan = forms.DateField(required=False, widget=forms.DateInput(attrs={'type': 'date'}))
    jam_kegiatan = forms.TimeField(required=False, widget=forms.TimeInput(attrs={'type':'time'}))
    nama_kegiatan = forms.CharField(max_length = 50, required=True, widget=forms.TextInput(attrs=nama_kegiatan_attrs))
    tempat_kegiatan = forms.CharField(max_length = 50, required=True, widget=forms.TextInput(attrs=tempat_kegiatan_attrs))
    kategori = forms.ChoiceField(choices=OPTION)

    