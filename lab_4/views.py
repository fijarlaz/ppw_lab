from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
from django.http import HttpResponseRedirect
# Create your views here.

response ={}
def index(request):
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    html= 'index.html'
    return render(request, html, response)

def about(request):
    return render(request, 'about.html')

def experiences(request):
    return render(request, 'experiences.html')

def contact(request):
    return render(request, 'contact.html')

def schedule(request):
    response['title'] = 'Make Schedule'
    response['add_schedule'] = ScheduleForm
    html = 'schedule.html'
    return render(request, html, response)

def schedule_save(request):
    form = ScheduleForm(request.POST or None)
    if (request.method == "POST"):
        if (form.is_valid()):
            response['hari_kegiatan'] = request.POST.get('hari_kegiatan')
            response['waktu_kegiatan'] = request.POST.get('waktu_kegiatan')
            response['jam_kegiatan'] = request.POST.get('jam_kegiatan')
            response['nama_kegiatan'] = request.POST['nama_kegiatan'] if request.POST['nama_kegiatan'] != '' else 'Anonymous'
            response['tempat_kegiatan'] = request.POST['tempat_kegiatan']
            response['kategori'] = request.POST['kategori']
            sched = Schedule(hari_kegiatan=response['hari_kegiatan'],waktu_kegiatan=response['waktu_kegiatan'],jam_kegiatan=response['jam_kegiatan'], nama_kegiatan=response['nama_kegiatan'], tempat_kegiatan=response['tempat_kegiatan'], kategori=response['kategori'])
            sched.save()
            schedule = Schedule.objects.all()
            response['schedule'] = schedule
            html = 'form_result.html'
            return render(request, html,response)
        else:
            return render(request, 'schedule.html', response)
    else:
        response['form'] = form
        return render(request, 'form_result.html', response)

def delete_schedule(request):
    delete_all = Schedule.objects.all().delete()
    return HttpResponseRedirect('/')


def exp(request):
    return render(request, 'exp.html')
